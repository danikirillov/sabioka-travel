package com.mygame.very.indy.programmers.sabioka.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygame.very.indy.programmers.sabioka.SabiokaMain;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(SabiokaMain.getInstance(), config);
	}

	@Override
	public void startActivity(android.content.Intent intent) {

	}

	@Override
	public android.view.WindowManager getWindowManager() {
		return null;
	}
}
