package com.mygame.very.indy.programmers.sabioka;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygame.very.indy.programmers.sabioka.Screens.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;

public class SabiokaMain extends Game {

	private float ppuX, ppuY;
	private HashMap<String,TextureRegion> textureRegions;
	private ShapeRenderer shape;
	private BitmapFont font;
	private SpriteBatch batch;
	private MainMenuScreen menuScreen;
	private GameScreen gameScreen;
	private SettingsScreen settingsScreen;
	private HighscoreScreen highscoreScreen;
	private static SabiokaMain instance=new SabiokaMain();

	public static SabiokaMain getInstance(){
		return instance;
	}

	private SabiokaMain(){}


	@Override
	public void create () {
		ppuX= Gdx.graphics.getWidth()/1334f;
		ppuY=Gdx.graphics.getHeight()/710f;
		loadGraphics();
		batch = new SpriteBatch();
		menuScreen=new MainMenuScreen(batch,textureRegions);
		try {
		gameScreen=new GameScreen(batch,textureRegions);
		} catch (FileAlreadyExistsException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		settingsScreen=new SettingsScreen(batch, textureRegions);
		highscoreScreen=new HighscoreScreen(batch,textureRegions);
		setScreen(menuScreen);
	}

	private void loadGraphics(){
		textureRegions=new HashMap<String, TextureRegion>();
		textureRegions.put("Sabioka",new TextureRegion(new Texture("android/assets/SbDogUltra2.0.png")));
		textureRegions.put("Play button", new TextureRegion(new Texture("android/assets/BtnPlayUltra.png")));
		textureRegions.put("Settings button", new TextureRegion(new Texture("android/assets/BtnSettings.png")));
		textureRegions.put("Scores button", new TextureRegion(new Texture("android/assets/BtnSoresUltra.png")));
		textureRegions.put("Back color", new TextureRegion(new Texture("android/assets/cells.png"), 2, 0, 1, 1));
		textureRegions.put("White color",new TextureRegion(new Texture("android/assets/cells.png"),1,0,1,1));
		textureRegions.put("Black color", new TextureRegion(new Texture("android/assets/cells.png"), 2, 0, 1, 1));
		textureRegions.put("On button", new TextureRegion(new Texture("android/assets/btnOn.png")));
		textureRegions.put("Off button",new TextureRegion(new Texture("android/assets/btnOff.png")));
		textureRegions.put("En button", new TextureRegion(new Texture("android/assets/btnEN.png")));
		textureRegions.put("Ru button", new TextureRegion(new Texture("android/assets/btnRU.png")));
		textureRegions.put("Back button", new TextureRegion(new Texture("android/assets/btnBack.png")));
	}

	public void showGame(){
		setScreen(gameScreen);
	};

	public void showMenu(){
		setScreen(menuScreen);
	}

	public void showSettings(){
		setScreen(settingsScreen);
	}

	public void showHighscoreScreen(){
		setScreen(highscoreScreen);
	}
	@Override
	public void render () {
		super.render();
	}

	public float getPpuX() {
		return ppuX;
	}

	public void setPpuX(float ppuX) {
		this.ppuX = ppuX;
	}

	public float getPpuY() {
		return ppuY;
	}

	public void setPpuY(float ppuY) {
		this.ppuY = ppuY;
	}

	public HashMap<String, TextureRegion> getTextures(){
		return textureRegions;
	}

	public ShapeRenderer getShape(){
		return shape;
	}

	public BitmapFont getFont(){
		return font;
	}
}
