package com.mygame.very.indy.programmers.sabioka.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygame.very.indy.programmers.sabioka.Model.World;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;

/**
 * Created by KirillovDaniel on 31.01.2016.
 */
public class GameScreen implements Screen {

    World world;


    public GameScreen(SpriteBatch batch, HashMap<String,TextureRegion> textureRegions) throws FileNotFoundException, FileAlreadyExistsException {
        loadGraphics();
        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false,Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        world=new World(new ScreenViewport(camera), batch, textureRegions);

    }

    private void loadGraphics(){}

    @Override
    public void show() {
        Gdx.input.setInputProcessor(world);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        world.dispose();
    }
}
