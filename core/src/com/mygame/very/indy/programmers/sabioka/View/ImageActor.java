package com.mygame.very.indy.programmers.sabioka.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygame.very.indy.programmers.sabioka.SabiokaMain;

/**
 * Created by KirillovDaniel on 31.01.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * SabiokaMain.getInstance().getPpuX(), y * SabiokaMain.getInstance().getPpuY());
        setSize(width * SabiokaMain.getInstance().getPpuX(), height * SabiokaMain.getInstance().getPpuY());
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public void draw(Batch batch, float parentAlpha){
        batch.draw(img,getX(),getY(),getWidth(),getHeight());
    }
}
