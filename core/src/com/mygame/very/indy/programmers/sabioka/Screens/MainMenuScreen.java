package com.mygame.very.indy.programmers.sabioka.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygame.very.indy.programmers.sabioka.Controllers.MoveToGame;
import com.mygame.very.indy.programmers.sabioka.Controllers.MoveToHighscoreScreen;
import com.mygame.very.indy.programmers.sabioka.Controllers.MoveToSettings;
import com.mygame.very.indy.programmers.sabioka.View.ImageActor;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 31.01.2016.
 */
public class MainMenuScreen implements Screen {

    private Stage stage;
    private ImageActor blackRect;
    private ImageActor whiteRect;
    private ImageActor sabioka;
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor settingsButton;
    private ImageActor highscoreButton;

    public MainMenuScreen(SpriteBatch batch, HashMap<String,TextureRegion> textureRegions){
        backGround=new ImageActor(textureRegions.get("Back color"),0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        blackRect = new ImageActor(textureRegions.get("Black color"),0,0,Gdx.graphics.getWidth(), 60);
        whiteRect = new ImageActor(textureRegions.get("White color"),0,0, Gdx.graphics.getWidth(), 59);
        sabioka = new ImageActor(textureRegions.get("Sabioka"),40,20);
        playButton=new ImageActor(textureRegions.get("Play button"),438,174);
        playButton.addListener(new MoveToGame());
        settingsButton=new ImageActor(textureRegions.get("Settings button"),910,142);
        settingsButton.addListener(new MoveToSettings());
        highscoreButton=new ImageActor(textureRegions.get("Scores button"),910,400);
        highscoreButton.addListener(new MoveToHighscoreScreen());

        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage=new Stage(new ScreenViewport(camera),batch);
        stage.addActor(backGround);
        stage.addActor(blackRect);
        stage.addActor(whiteRect);
        stage.addActor(sabioka);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
        stage.addActor(highscoreButton);
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
