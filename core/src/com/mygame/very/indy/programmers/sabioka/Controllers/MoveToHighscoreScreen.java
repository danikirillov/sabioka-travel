package com.mygame.very.indy.programmers.sabioka.Controllers;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygame.very.indy.programmers.sabioka.SabiokaMain;

/**
 * Created by KirillovDaniel on 31.01.2016.
 */
public class MoveToHighscoreScreen extends ClickListener {
    public void clicked(InputEvent event, float x, float y){
        SabiokaMain.getInstance().showHighscoreScreen();
    }
}
