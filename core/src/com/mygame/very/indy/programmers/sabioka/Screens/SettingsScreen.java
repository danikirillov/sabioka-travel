package com.mygame.very.indy.programmers.sabioka.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygame.very.indy.programmers.sabioka.Controllers.MoveToMenu;
import com.mygame.very.indy.programmers.sabioka.View.ImageActor;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 31.01.2016.
 */
public class SettingsScreen implements Screen {

    private Stage stage;
    private ImageActor backGround;
    private ImageActor btnOn;
    private ImageActor btnOff;
    private ImageActor btnEn;
    private ImageActor btnRu;
    private ImageActor btnBack;

    public SettingsScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions){
        backGround=new ImageActor(textureRegions.get("Back color"),0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        btnBack=new ImageActor(textureRegions.get("Back button"),1120,80);
        btnBack.addListener(new MoveToMenu());
        btnOn= new ImageActor(textureRegions.get("On button"),135,410);
        btnEn= new ImageActor(textureRegions.get("En button"),135,190);
        btnOff= new ImageActor(textureRegions.get("Off button"),350,410);
        btnRu= new ImageActor(textureRegions.get("Ru button"),350,190);
        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage=new Stage(new ScreenViewport(camera),batch);
        stage.addActor(backGround);
        stage.addActor(btnBack);
        stage.addActor(btnEn);
        stage.addActor(btnOff);
        stage.addActor(btnOn);
        stage.addActor(btnRu);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
